//file for defining 'public' functions for the library

/*
 *	void lcd_init();
 *
 *	Use this command to allow the library to initialise communications with
 *	the LCD display. LCD control is on pins 0-3 of PORTB.
 *
 *
 *	void lcd_clear();
 *
 *	This command clears any text that is currently on the LCD display,
 *	returning the cursor to the leftmost position of line 1.
 *
 *
 *	lcd_display(char* string_p);
 *
 *	This command will write the string that string_p is pointing to to the
 *	LCD display. '\n' characters are treated as a new line, and the method
 *	will stop when it has written 32 characters (two lines) or reaches a
 *	'\0' null terminator byte in the string.
 */
extern void lcd_init(void);
extern void lcd_clear();
extern void lcd_display(char* string_p);

