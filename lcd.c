/*
 * Program: lcd.c
 * Author:  S. J. Gosnell
 *
 * This program is both a LCD demo and Library
 */

//included for delay functions
#include <util/delay.h>

//included for AVR functions
#include <avr/io.h>

//included for sprintf
#include <stdio.h>

#ifdef LIBRARY
#include "lcd.h"
#endif

//PORT C DEFINITIONS
	//enable
#define LCD_EN 0x08
#define LCD_RDY 0x01
	//read
#define LCD_READ 0x04
#define LCD_WRITE 0x00
	//register select (data/command mode)
#define LCD_DATA 0x20
#define LCD_COMMAND 0x00

//the header file for this program if it is a library
#define LCD_DATA_PORT PORTC
#define LCD_DATA_IN PINC 
#define LCD_DATA_DDR DDRC

//COMMAND DEFINITIONS
#define LCD_CLEAR 0x01
#define LCD_CURSOR_HOME 0x02
#define LCD_CURSOR_LINE_1 0x80
#define LCD_CURSOR_LINE_2 0xC0
#define LCD_DISPLAY_OFF 0x08
#define LCD_DISPLAY_ON_BLINK 0x0F
#define LCD_DISPLAY_ON 0x0E
#define LCD_STATIC_DISPLAY 0x06
#define LCD_8_BIT_BUS 0x39

#define LEN_LINE 16

static void PulseEnable()
{
	LCD_DATA_PORT |= LCD_EN;
	_delay_us(10);
	LCD_DATA_PORT &= ~LCD_EN;

}

static void PulseClock()
{
	LCD_DATA_PORT |= 0x02;
	_delay_us(100);
	LCD_DATA_PORT &= ~0x02;
	_delay_us(1);
}

static void Await_LCD_Ready()
{
	//set the R/~w bit, to allow for reading the ready bit
	LCD_DATA_PORT |= LCD_READ;
	LCD_DATA_PORT &= ~LCD_RDY;	
	//set pin 1 on LCD_DATA_PORT as a digital input
	LCD_DATA_DDR &= ~LCD_RDY;

	_delay_us(10);
	
	//lock in a loop until pin B1 goes low, signaling the LCD is ready for
	//a command
	while(1)
	{

		PulseEnable();
		if(!(LCD_DATA_IN & LCD_RDY))
			break;
	}

	_delay_us(10);
	//re-set pin 1 on LCD_DATA_PORT
	LCD_DATA_DDR |= LCD_RDY;
	
	//reset the R/~W bit
	LCD_DATA_PORT &= ~LCD_READ;
	//at this point, the LCD is ready to accept the next instruction
}


static void LCD_Send(char data, char RS, int skip_sense)
{
	//variable for loop counter
	int i = 0;
	char bit7 = 0;
	
	if(data&0x80)
		bit7 = 0x10;
	
	//load the values into the shift register
	//Shift register is on LCD_DATA_PORT, pin 0
	if(!skip_sense)
		Await_LCD_Ready();
	

	//load into the register the RS bit
		//a 1 in this bit indicates data mode
		//a 0 in this bit indicates instruction mode
	if(RS)
	{
		LCD_DATA_PORT |= 0x01;
	}
	else
	{
		LCD_DATA_PORT &= ~0x01;
	}
	//load the RS bit into the register.
	PulseClock();

	//load the lowest bit of data into the register first
	for(i = 0; i<7; i++)
	{
		LCD_DATA_PORT &= ~0x01;
		LCD_DATA_PORT |= data & 0x01;
		//load the data into the register
		PulseClock();
		//shift the data to the right, to load the next bit
		data = data>>1;
	}
	
	
	//write the final bit to LCD_DATA_PORT, then send all data to the LCD dispaly.
	
	LCD_DATA_PORT &= ~LCD_RDY;
	
	//ensure that the bit is written
	//while((LCD_DATA_IN&LCD_RDY) != (data&0x01))
		//LCD_DATA_PORT |= (data&0x01)<<4;
	LCD_DATA_PORT |= 0x20;
	//LCD_DATA_PORT |= bit7;
	
	//signal to the LCD that data is ready for it
	PulseEnable();
}


void lcd_init(void)
{
	//set ports C and D as outputs
	LCD_DATA_DDR = 0x0F;
	//clear all low bits
	LCD_DATA_PORT &= ~0x0F;

	//init procedure
	_delay_ms(500);
	LCD_Send(LCD_8_BIT_BUS, LCD_COMMAND, 1);
	_delay_ms(5);
	PulseEnable();
	_delay_ms(1);
	PulseEnable();
	_delay_ms(50);

	//block of settings

	//8 bit bus mode
	LCD_Send(LCD_8_BIT_BUS, LCD_COMMAND, 0);
	//Display off
	LCD_Send(LCD_DISPLAY_OFF, LCD_COMMAND, 0);
	//Clear Display
	LCD_Send(LCD_CLEAR, LCD_COMMAND, 0);
	//Set cursor to increment, prevent screen scrolling
//	LCD_Send(LCD_SCROLL_DISPLAY, LCD_COMMAND, 0);
	LCD_Send(LCD_STATIC_DISPLAY, LCD_COMMAND, 0);
	//Screen on, cursor blink
	LCD_Send(LCD_DISPLAY_ON_BLINK, LCD_COMMAND, 0);

	//at this point, the LDC is in 8 bit mode, with the cursor blinking, ready for input
}

void lcd_clear()
{
	//set the cursor to position 0 on line 1.
	LCD_Send(LCD_CLEAR, LCD_COMMAND, 0);
}

static void lcd_new_line(int pos)
{
	//LCD_Send(LCD_CURSOR_LINE_2, LCD_COMMAND, 0);
	//LCD_Send(0x85, LCD_COMMAND, 0);
	while(pos<0x27)
	{
		LCD_Send(' ', LCD_DATA, 0);
		pos++;
	}
}
static void clearbuffer()
{
	int i;
	LCD_DATA_PORT &=0;
	for(i = 0; i<10; i++)
	{
		//load the data into the register
		PulseClock();
	}
}

//returns 0 if all data in string_p was written to display
static int lcd_write_line(char *string_p)
{
	int count = 1;

	LCD_Send(*string_p, LCD_DATA, 0);
	string_p++;

	while(*(string_p-1))
	{
		//end the line at a new line character
		if(*string_p == '\n') return count;
		LCD_Send(*string_p, LCD_DATA, 0);
		string_p++;
		//if at the end of the line, and not the final char
		//let the caller know that the whole string was not processed
		if(count == LEN_LINE) return count;
		count++;
	}

	//let the caller know that all data was written to display
	return 0;

}

void lcd_display(char *string_p)
{
	int position = 0;
	lcd_clear();
	//set the cursor to position 0 on line 1.
	LCD_Send(LCD_CURSOR_HOME, LCD_COMMAND, 0);

	//write one line to the LCD
	position = lcd_write_line(string_p);
	
	//if the data is split accross lines, write a second line to the
	//display
	if(position)
	{
		lcd_new_line(position);
		string_p += position;
		lcd_write_line(string_p);
	}
}

#ifndef LIBRARY /*Not Library Code*/
int main(void)
{
	int i;
	char buff[200];
	//initialise a buffer of 11 characters
    	lcd_init();
	
/*	LCD_DATA_PORT &= ~0x10;

    	while (1)
    	{
    		for(i=0; i<255; i++)
    		{
    			sprintf(buff, "Count: %d", i);
			lcd_display(buff);
			_delay_ms(100);
	    	}
    	}*/



    while (1)
	{
		sprintf(buff,"This string is two lines long");
		lcd_display(buff);
		_delay_ms(1000);
		sprintf(buff, "This string isnt");
		lcd_display(buff);
		_delay_ms(1000);
	}


    	return 0;
}
#endif /*Not Library Code*/
