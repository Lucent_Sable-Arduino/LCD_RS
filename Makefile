#variable for clock frequency of the arduino
cf=16000000Ul
#the microcontroller to use
uc=atmega328p
UC=ATMEGA328P
#name of file to compile
file=lcd
ex=example
libs= lcd
libpath= .
hpath =  
#device to upload to
dev=/dev/ttyACM0
#baud-rate to upload at
baud=115200

#Targets that make will accept
all:upload

compile:$(file).o

library:lib$(file).a
lib:lib$(file).a

example:uploadex
ex:uploadex

#tasks needed to carry out these targets

###Upload the simple Program
#compile the file if file.c has been modified
$(file).o:$(file).c
	avr-gcc -Os -DF_CPU=$(cf) -mmcu=$(uc) -c -o $(file).o $(file).c

#generate the raw file if file.0 has been modified
$(file).raw:$(file).o
	avr-gcc -mmcu=$(uc) $(file).o -o $(file).raw

#generate the hex file if the raw file has been modified
$(file).hex:$(file).raw
	avr-objcopy -O ihex -R .eeprom $(file).raw $(file).hex

#upload the file to the board
upload:$(file).hex
	avrdude -F -c arduino -p $(UC) -P $(dev) -b $(baud) -U flash:w:$(file).hex




###Upload the library demo program
$(ex).o:$(ex).c
	avr-gcc -Os $(hpath:%=-I%) -DF_CPU=$(cf) -mmcu=$(uc) -c -o $(ex).o $(ex).c

$(ex).raw:$(ex).o
	avr-gcc -mmcu=$(uc) $(ex).o -o $(ex).raw $(libpath:%=-L%) $(libs:%=-l%)

$(ex).hex:$(ex).raw
	avr-objcopy -O ihex -R .eeprom $(ex).raw $(ex).hex

uploadex:$(ex).hex
	avrdude -F -c arduino -p $(UC) -P $(dev) -b $(baud) -U flash:w:$(ex).hex




###Compile the library
lib$(file).o:$(file).c
	avr-gcc -Os -DLIBRARY -DF_CPU=$(cf) -mmcu=$(uc) -c -o lib$(file).o $(file).c

lib$(file).a:lib$(file).o
	avr-ar -rcs lib$(file).a lib$(file).o	
