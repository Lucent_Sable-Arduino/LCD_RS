/*
 * Program: lcd.c
 * Author:  S. J. Gosnell
 *
 * This program is both a LCD demo and Library
 */

//included for delay functions
#include <util/delay.h>

//included for sprint
#include <stdio.h>

//included for lcd library functions
#include "lcd.h"

int main(void)
{
	int i;
	char buff[200];
    	lcd_init();

    	while (1)
    	{
    		for(i=0; i<255; i++)
    		{
    			sprintf(buff, "Count:%d\nAgain:%d", i, i);
			lcd_display(buff);
			_delay_ms(100);
	    	}
    	}

    	return 0;
}
